/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.cognito.auth;

import org.apache.shiro.authc.AuthenticationInfo;

/**
 *
 * @author micha
 */
public class CognitoAuthenticationInfo {
    
    private AuthenticationInfo authentication;
    private String accessToken;
    
    public CognitoAuthenticationInfo (AuthenticationInfo auth, String token) {
        this.authentication = auth;
        this.accessToken = token;
    }

    public AuthenticationInfo getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationInfo authentication) {
        this.authentication = authentication;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String token) {
        this.accessToken = token;
    }
}
