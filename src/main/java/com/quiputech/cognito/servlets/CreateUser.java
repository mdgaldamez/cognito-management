/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.cognito.servlets;

import com.quiputech.cognito.auth.Constants;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.kdgcommons.lang.StringUtil;

/**
 *
 * @author micha
 */
public class CreateUser extends AbstractCognitoServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        String email = request.getParameter(Constants.RequestParameters.EMAIL);
        String firstName = request.getParameter(Constants.RequestParameters.NOMBRE);
        String lastName = request.getParameter(Constants.RequestParameters.APELLIDO);
        if (StringUtil.isBlank(email) || StringUtil.isBlank(firstName) || StringUtil.isBlank(lastName))
        {
            reportResult(response, Constants.ResponseMessages.INVALID_REQUEST);
            return;
        }
        try {
            boolean created = CognitoManager.createCognitoAccount(email, firstName, lastName);
           
            if (!created) {
                System.out.println("Account not created");
                logger.debug("{} user already exists", email);
                reportResult(response, Constants.ResponseMessages.USER_ALREADY_EXISTS);
                RequestDispatcher rd = request.getRequestDispatcher("index.html");
                rd.forward(request, response);
            } else {
                System.out.println(email + " created");
                //
            //    CognitoManager.addUserToGroup(email, CognitoAccount.MERCHANT);
                reportResult(response, Constants.ResponseMessages.FORCE_PASSWORD_CHANGE);
                RequestDispatcher rd = request.getRequestDispatcher("confirm-signup.html");
                rd.forward(request, response); 
            }
        }catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        } 
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
