package com.quiputech.cognito.servlets;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.AdminAddUserToGroupRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminCreateUserResult;
import com.amazonaws.services.cognitoidp.model.AdminDisableUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminEnableUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthRequest;
import com.amazonaws.services.cognitoidp.model.AdminInitiateAuthResult;
import com.amazonaws.services.cognitoidp.model.AdminListGroupsForUserRequest;
import com.amazonaws.services.cognitoidp.model.AdminListGroupsForUserResult;
import com.amazonaws.services.cognitoidp.model.AdminResetUserPasswordRequest;
import com.amazonaws.services.cognitoidp.model.AdminUpdateUserAttributesRequest;
import com.amazonaws.services.cognitoidp.model.AttributeType;
import com.amazonaws.services.cognitoidp.model.AuthFlowType;
import com.amazonaws.services.cognitoidp.model.DeliveryMediumType;
import com.amazonaws.services.cognitoidp.model.GetUserRequest;
import com.amazonaws.services.cognitoidp.model.GetUserResult;
import com.amazonaws.services.cognitoidp.model.GroupType;
import com.amazonaws.services.cognitoidp.model.ConfirmForgotPasswordRequest;
import com.quiputech.cognito.auth.CognitoAccount;
import com.quiputech.cognito.auth.CognitoAuthenticationInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.util.CollectionUtils;
import org.apache.shiro.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author giancarlo
 */
public class CognitoManager extends AuthorizingRealm {

    private static final Logger LOGGER = LoggerFactory.getLogger(CognitoManager.class);
    protected static final String USERPOOLID = System.getProperty("COGNITO_USER_POOL_ID", "us-east-1_2cJ1Se1fI"); //"us-east-1_nkDoJq4J4"); // "us-east-1_8LntJGJFC");
    protected static final String CLIENTID = System.getProperty("COGNITO_CLIENT_ID", "10lv0617o5dic51ebsnqeiijb7"); // "6mret6v67lrgd45cuel9l70t8e"); // "j0kpn35m0jr1gkkjef7vkl62c");
    protected static final String TEMPORARYPASSWORD = "xl9Stg19#";
    protected static final BasicAWSCredentials AWS_CREDENTIALS = new BasicAWSCredentials(CLIENTID, USERPOOLID);
    protected static AWSCognitoIdentityProvider cognitoClient = AWSCognitoIdentityProviderClientBuilder
            .standard()
            .withCredentials(InstanceProfileCredentialsProvider.getInstance())
            .build();
    public static GetUserResult getUser(String accessToken) {
        GetUserRequest authRequest = new GetUserRequest().withAccessToken(accessToken);
        return cognitoClient.getUser(authRequest);
    }

    public static AdminInitiateAuthResult refreshToken(String refreshToken) {
        Map<String, String> authParams = new HashMap<>();
        authParams.put("REFRESH_TOKEN", refreshToken);

        AdminInitiateAuthRequest refreshRequest = new AdminInitiateAuthRequest()
                .withAuthFlow(AuthFlowType.REFRESH_TOKEN)
                .withAuthParameters(authParams)
                .withClientId(CLIENTID)
                .withUserPoolId(USERPOOLID);

        return cognitoClient.adminInitiateAuth(refreshRequest);
    }

    public static CognitoAccount authenticateAccount(String userName, String password) {
        CognitoAccount account = null;
        try {
            AuthenticationToken token = new UsernamePasswordToken(userName, password);
            CognitoAuthenticationInfo auth = authenticate(token);
            SimpleAccount simpleAccount = (SimpleAccount) auth.getAuthentication();

            if (simpleAccount != null) {
                List<String> roles = getRoles(userName);
                account = new CognitoAccount();
                account.setEmail(userName);
                account.setGroups(roles);
                account.setName(userName);
                account.setAccessToken(auth.getAccessToken());

                getUserAttributes(userName, auth.getAccessToken());
            }

        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }

        return account;
    }

    public static boolean addUserToGroup(String email, String group) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminAddUserToGroupRequest cognitoRequest = new AdminAddUserToGroupRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email)
                    .withGroupName(group);

            cognitoClient.adminAddUserToGroup(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            result = false;
        }

        return result;
    }

    public static boolean createCognitoAccount(String email, String firstName, String lastName) {
        boolean result = false;
        System.out.println(String.format("%s - %s - %s", email, firstName, lastName));
        System.out.println(String.format("%s - %s", USERPOOLID, CLIENTID));
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email)
                    .withUserAttributes(
                            new AttributeType()
                                    .withName("email")
                                    .withValue(email),
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true"),
                            new AttributeType()
                                    .withName("given_name")
                                    .withValue(firstName),
                            new AttributeType()
                                    .withName("family_name")
                                    .withValue(lastName)
                    )
                    .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                    .withForceAliasCreation(Boolean.FALSE);

            AdminCreateUserResult response = cognitoClient.adminCreateUser(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
            result = false;
        }

        return result;
    }

    public static void updateStatusAccount(String email, boolean status) {
        if (status) {
            AdminEnableUserRequest enableRequest = new AdminEnableUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminEnableUser(enableRequest);
        } else {
            AdminDisableUserRequest disableRequest = new AdminDisableUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminDisableUser(disableRequest);
        }
    }

    public static boolean updateCognitoAccount(String serviceId, String email, String firstName, String lastName, boolean status) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminUpdateUserAttributesRequest cognitoRequest = new AdminUpdateUserAttributesRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email)
                    .withUserAttributes(
                            new AttributeType()
                                    .withName("email")
                                    .withValue(email),
                            new AttributeType()
                                    .withName("email_verified")
                                    .withValue("true"),
                            new AttributeType()
                                    .withName("given_name")
                                    .withValue(firstName),
                            new AttributeType()
                                    .withName("family_name")
                                    .withValue(lastName),
                            new AttributeType()
                                    .withName("profile")
                                    .withValue(serviceId)
                    );

            cognitoClient.adminUpdateUserAttributes(cognitoRequest);

            updateStatusAccount(email, status);

            result = true;
        } catch (Exception ex) {
            result = false;
        }

        return result;
    }
    
        public static boolean resetCognitoPassword(String email) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }

        try {
            AdminResetUserPasswordRequest cognitoRequest = new AdminResetUserPasswordRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminResetUserPassword(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            result = false;
        }

        return result;
    }

    public static boolean ConfirmResetCognitoPassword(String email, String code, String password) {
        boolean result = false;
        if (email == null) {
            return result;
        } else if (email.isEmpty()) {
            return result;
        }
        
        try {
            ConfirmForgotPasswordRequest cognitoRequest = new ConfirmForgotPasswordRequest()
                    .withClientId(CLIENTID)
                    .withConfirmationCode(code)
                    .withPassword(password)
                    .withUsername(email);

            cognitoClient.confirmForgotPassword(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            result = false;
        }

        return result;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();

        AdminListGroupsForUserRequest groupRequest = new AdminListGroupsForUserRequest()
                .withUsername(username)
                .withUserPoolId(USERPOOLID);

        AdminListGroupsForUserResult groupResult = cognitoClient.adminListGroupsForUser(groupRequest);
        Set<String> roles = new HashSet<>();
        if (groupResult != null) {
            for (GroupType g : groupResult.getGroups()) {
                roles.add(g.getGroupName());
            }
        }

        info.addRoles(roles);
        return info;
    }

    private static List<String> getRoles(String username) {
        AdminListGroupsForUserRequest groupRequest = new AdminListGroupsForUserRequest()
                .withUsername(username)
                .withUserPoolId(USERPOOLID);

        AdminListGroupsForUserResult groupResult = cognitoClient.adminListGroupsForUser(groupRequest);
        List<String> roles = new ArrayList<>();
        if (groupResult != null) {
            for (GroupType g : groupResult.getGroups()) {
                roles.add(g.getGroupName());
            }
        }

        return roles;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        return authenticate(token).getAuthentication();
    }

    private static CognitoAuthenticationInfo authenticate(AuthenticationToken token) throws AuthenticationException {
        final String password = new String((char[]) token.getCredentials());
        final String username = (String) token.getPrincipal();

        Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", username);
        authParams.put("PASSWORD", password);

        AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
                .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withAuthParameters(authParams)
                .withClientId(CLIENTID)
                .withUserPoolId(USERPOOLID);

        AdminInitiateAuthResult authResponse = cognitoClient.adminInitiateAuth(authRequest);

        if (authResponse.getChallengeName() == null) {
            PrincipalCollection principals = null;
            String authtoken = "";
            try {
                authtoken = authResponse.getAuthenticationResult().getAccessToken();
                principals = createPrincipals(username, authtoken);
            } catch (Exception e) {
                throw new AuthenticationException("Unable to obtain authenticated account properties.", e);
            }
            SimpleAccount account = new SimpleAccount(principals, token.getCredentials());

            return new CognitoAuthenticationInfo(account, authtoken);
        }

        return null;
    }

    @Override
    protected Object getAuthenticationCacheKey(PrincipalCollection principals) {
        if (!CollectionUtils.isEmpty(principals)) {
            Collection thisPrincipals = principals.fromRealm(getName());
            if (CollectionUtils.isEmpty(thisPrincipals)) {
                //no principals attributed to this particular realm.  Fall back to the 'master' primary:
                return principals.getPrimaryPrincipal();

            } else {
                Iterator iterator = thisPrincipals.iterator();
                iterator.next(); //First item is the Stormpath' account href
                //Second item is Stormpath' account map
                Map<String, Object> accountInfo = (Map<String, Object>) iterator.next();
                //Users can indistinctively login using their emails or usernames. Therefore, we need to try which is
                //the key used in each case
                String email = (String) accountInfo.get("email");
                if (getAuthenticationCache().get(email) != null) {
                    return email;
                }
                return accountInfo.get("username");
            }
        }
        return null;
    }

    private static void nullSafePut(Map<String, String> props, String propName, String value) {
        String cleanValue = StringUtils.clean(value);
        if (cleanValue != null) {
            props.put(propName, cleanValue);
        }
    }

    protected static PrincipalCollection createPrincipals(String username, String accessToken) {

        Collection<Object> principals = new ArrayList<>(2);
        principals.add(username);
        principals.add(getUserAttributes(username, accessToken));

        return new SimplePrincipalCollection(principals, CognitoManager.class.getName());
    }

    private static Map<String, String> getUserAttributes(String username, String accesstoken) {
        Map<String, String> props = new LinkedHashMap<>();

        nullSafePut(props, "username", username);
        nullSafePut(props, "email", username);

        GetUserRequest userRequest = new GetUserRequest().withAccessToken(accesstoken);
        GetUserResult userResponse = cognitoClient.getUser(userRequest);

        for (AttributeType att : userResponse.getUserAttributes()) {
            nullSafePut(props, att.getName(), att.getValue());
        }

        return props;
    }

}
