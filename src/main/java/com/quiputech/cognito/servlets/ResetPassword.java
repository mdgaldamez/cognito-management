// Copyright (c) Keith D Gregory, all rights reserved
package com.quiputech.cognito.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.amazonaws.services.cognitoidp.model.*;
import com.quiputech.cognito.auth.Constants;
import static com.quiputech.cognito.servlets.CognitoManager.CLIENTID;
import static com.quiputech.cognito.servlets.CognitoManager.cognitoClient;
import net.sf.kdgcommons.lang.StringUtil;
import net.sf.kdgcommons.lang.ThreadUtil;


/**
 *  This servlet finishes the signup process for a new user, changing the temporary
 *  password to a final password.
 */

public class ResetPassword extends AbstractCognitoServlet
{
    private static final long serialVersionUID = 1L;


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
    {
        String emailAddress = request.getParameter(Constants.RequestParameters.EMAIL);
        String code = request.getParameter(Constants.RequestParameters.CODE);
        String finalPassword = request.getParameter(Constants.RequestParameters.PASSWORD);
        if (StringUtil.isBlank(emailAddress) || StringUtil.isBlank(code) || StringUtil.isBlank(finalPassword))
        {
            reportResult(response, Constants.ResponseMessages.INVALID_REQUEST);
            return;
        }

        logger.debug("confirming reset password of user {}", emailAddress);

        try
        {
            ConfirmForgotPasswordRequest cognitoRequest = new ConfirmForgotPasswordRequest()
                    .withClientId(CLIENTID)
                    .withConfirmationCode(code)
                    .withPassword(finalPassword)
                    .withUsername(emailAddress);

            cognitoClient.confirmForgotPassword(cognitoRequest);
            reportResult(response, Constants.ResponseMessages.PASSWORD_CHANGED);
        }
        catch(InvalidParameterException | InvalidPasswordException ex){
            logger.debug("{} submitted invalid password", emailAddress);
            reportResult(response, Constants.ResponseMessages.INVALID_PASSWORD);
        }
        catch(CodeMismatchException | ExpiredCodeException ex){
            logger.debug("{} code is wrong", emailAddress);
            reportResult(response, Constants.ResponseMessages.INVALID_CODE);
        }
        catch (UserNotConfirmedException | UserNotFoundException ex)
        {
            logger.debug("not found: {}", emailAddress);
            reportResult(response, Constants.ResponseMessages.NO_SUCH_USER);
        }
        catch (NotAuthorizedException ex)
        {
            logger.debug("invalid credentials: {}", emailAddress);
            reportResult(response, Constants.ResponseMessages.NO_SUCH_USER);
        }
        catch (TooManyRequestsException ex)
        {
            logger.warn("caught TooManyRequestsException, delaying then retrying");
            ThreadUtil.sleepQuietly(250);
            doPost(request, response);
        }
    }


    @Override
    public String getServletInfo()
    {
        return "Handles second stage of user signup, replacing temporary password by final";
    }

}
