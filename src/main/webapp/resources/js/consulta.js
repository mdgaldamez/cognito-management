/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $('#Jtabla').DataTable({
        //responsive: true
    });
 
    $('.form_datetime').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        forceParse: 0,
        showMeridian: 1
    });
    $('.form_date').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.form_time').datetimepicker({
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0
    });
    
    $('#btnPrevious').click(function() {
        var currentPage = parseInt($("#currentPage").val());
        $("#currentPage").val(currentPage > 1 ? currentPage - 1 : 1);
        
    });
    
    $('#btnNext').click(function() {
        var currentPage = parseInt($("#currentPage").val());
        var endPage = parseInt($("#endPage").val());
        $("#currentPage").val(currentPage < endPage ? currentPage + 1 : endPage);
        
    });
    
    $('#btnFirst').click(function() {
        $("#currentPage").val(1);   
    });
    
    $('#btnLast').click(function() {
        $("#currentPage").val(parseInt($("#endPage").val()));      
    });
    
    $('.grid_pager').click(function() {
        var currentPage = parseInt($("#currentPage").val());
        var end = parseInt($("#endPage").val());
        $("#totalPages").html(currentPage + " de " + end);
        //var ix = parseInt($(this).text()) - 1;
        var ix = currentPage - 1;
        console.info(ix);
        $('.grid_row').hide();
        $('.page_' + ix).show('now');
        //$('.grid_pager').removeClass('grid_pager_selected');
        //$(this).addClass('grid_pager_selected');        
    }); 
    
    $('.grid_row').hide();
    $('.page_0').show();
           
    var host = window.location.pathname,
    substring = "consultaGrupo";
    if (host.indexOf(substring) !== -1) {
        $("#btnGroupNew").show();
    } else {
        $("#btnGroupNew").hide();
    } 
    
    var host = window.location.pathname,
    substring = "ConsultaGrupoController";
    if (host.indexOf(substring) !== -1) {
        $("#btnGroupNew2").show();
    } else {
        $("#btnGroupNew2").hide();
    } 
    
    var host = window.location.pathname,
    substring = "usuarioGrupo";
    if (host.indexOf(substring) !== -1) {
        $("#btnUserNew").show();
    } else {
        $("#btnUserNew").hide();
    } 
    
    var host = window.location.pathname,
    substring = "UsuarioGrupoController";
    if (host.indexOf(substring) !== -1) {
        $("#btnUserNew2").show();
    } else {
        $("#btnUserNew2").hide();
    } 
    
    var host = window.location.pathname,
    substring = "usuarioAdministrador";
    if (host.indexOf(substring) !== -1) {
        $("#btnUserAdmin").show();
    } else {
        $("#btnUserAdmin").hide();
    } 
    
    //$('#tbx_lista').html($('#tbx_lista').html().trim());
    //$('#tbx_lista2').html($('#tbx_lista2').html().trim());
    
    //password.onchange = validatePassword;
    //confirm_password.onkeyup = validatePassword;
});