$(document).ready(function () {
    $("#cbx_comercio").click(function () {
        if (this.checked) {
            $("#tbx_comercio").prop('required', true);
            $("#tbx_comercio").prop('disabled', false);
        } else {
            $("#tbx_comercio").val("");
            $("#tbx_comercio").prop('required', false);
            $("#tbx_comercio").prop('disabled', true);
        }
    });
});